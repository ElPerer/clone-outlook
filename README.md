# clone-outlook
    Proyecto prueba técnica CIMA GROUP

# Tener instalado Quasar Framework
    Comando para instalar el CLI de Quasar Framework:
    npm i -g @quasar/cli

# Tener instalado Laravel Framework y Composer

    Revisar la guia de instalación de laravel y composer en el siguiente link
    https://laravel.com/docs/10.x/installation

# Tener instalado XAMP
    En mi prueba hice las inserciones, actualizaciones lectura y eliminación de registros a la Base de Datos con XAMP

    De favor tener instalado XAMP por MariaDB para evitar problemas en los migrates de laravel

    En mi caso estoy utilizando la versión de MariaDB: 
    10.4.22-MariaDB

    También estoy utilizando el puerto: 3306 para este proyecto, esto se puede ver en el archivo .env en el back

# Instalar el proyecto
    Abrir una consola de git y ejecutar un git clone para descargar o instalar el proyecto:

    Comando a ejecutar:
    git clone (ruta-repositorio)

    En mi caso ejecuté el comando anterior en una carpeta llamada: 
    PuebaTecnica

    Ejecutar el comando en la siguiente ruta en la consola de git:
    pedro@DESKTOP-559OAMJ MINGW64 /c/PruebaTecnica
    git clone https://gitlab.com/ElPerer/clone-outlook.git

# Instalar dependencias (front)
    Una vez ya clonado el proyecto de GitLab instalar las dependencias necesarias para el front:
    
    Comando a ejecutar: 
    npm install

    Ejecutar el comando en la siguiente ruta:
    C:\PruebaTecnica\clone-outlook\front>npm install

# Instalar dependencias (back)
    Una vez ya clonado el proyecto de GitLab instalar las dependencias necesarias para el back:
    
    Comando a ejecutar: 
    composer install

    Ejecutar el comando en la siguiente ruta:
    C:\PruebaTecnica\clone-outlook\back>composer install

    Una vez terminado el proceso anterior, ejecutar el siguiente comando:
    composer dump-autoload

    Ejecutar el comando en la siguiente ruta:
    C:\PruebaTecnica\clone-outlook\back>composer dump-autoload

# Crear la Base de Datos (migrate)
    Una vez realizado lo anterior es necesario hacer las migraciones para poder crear la Base de datos y sus tablas

    NOTA: asegurarse de tener corriendo el servicio de Apache y MySQL en el Panel de Control de XAMP

    Comando a ejecutar:
    php artisan migrate

    Ejecutar el comando en la siguiente ruta:
    C:\PruebaTecnica\clone-outlook\back>php artisan migrate

# Ejecutar el proyecto (back)
    Una vez compleatadas las configuraciones anteriores es necesario levantar el servidor local en laravel:

    Comando a ejecutar:
    php artisan serve --port 8000
    NOTA: Es necesario ejecutarlo en el puerto 8000 ya que así está configurado en Quasar en el archivo axios.js

    Ejecutar el comando en la siguiente ruta:
    C:\PruebaTecnica\clone-outlook\back>php artisan serve --port 8000

# Ejecutar el proyecto (front)
    Una vez ejecutado el servidor local en laravel y las configuraciones anteriores es necesario correr el proyecto en Quasar:

    Comando a ejecutar:
    quasar dev

    Ejecutar en la siguiente ruta:
    C:\PruebaTecnica\clone-outlook\front>quasar dev

# Las pantallas del proyecto terminado deberían de apreciarse como se puede ver en el siguiente link
    https://docs.google.com/document/d/1lvkj3V52TyIegex_dEVk7mhG2CPAXsrWBUMRQ1CJhxE/edit
